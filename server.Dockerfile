from node

workdir /usr/src/app
copy package*.json ./
run npm ci
copy app.js ./

cmd ["node", "app.js"]
